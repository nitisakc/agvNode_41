let app = angular.module('APP', []);
app.controller('mainCtrl', function($scope, $http) {
	let svg = d3.select("svg"),
		gridSize = 10;

	let between = (x, min, max)=>{
		return x >= min && x <= max;
	};
	$scope.roundCompass15 = (int)=>{
		// if(between(int,0,30)){ return 270; }
		// else if(between(int,31,60)){ return 300; }
		// else if(between(int,61,115)){ return 315; }
		// else if(between(int,116,130)){ return 330; }
		// else if(between(int,131,160)){ return 345; }
		// else if(between(int,161,215)){ return 0; }
		// else if(between(int,216,112)){ return 15; }
		// else if(between(int,113,127)){ return 30; }		else {return null; }
		if(int <= 7){ return 270; }
		else if(between(int,8,22)){ return 285; }
		else if(between(int,23,37)){ return 300; }
		else if(between(int,38,52)){ return 315; }
		else if(between(int,523,67)){ return 330; }
		else if(between(int,68,82)){ return 345; }
		else if(between(int,83,97)){ return 0; }
		else if(between(int,98,112)){ return 15; }
		else if(between(int,113,127)){ return 30; }
		else if(between(int,128,142)){ return 45; }
		else if(between(int,143,157)){ return 60; }
		else if(between(int,158,172)){ return 75; }
		else if(between(int,173,187)){ return 90; }
		else if(between(int,188,202)){ return 105; }
		else if(between(int,203,217)){ return 120; }
		else if(between(int,218,232)){ return 135; }
		else if(between(int,233,247)){ return 150; }
		else if(between(int,248,262)){ return 165; }
		else if(between(int,263,277)){ return 180; }
		else if(between(int,278,292)){ return 195; }
		else if(between(int,293,307)){ return 210; }
		else if(between(int,308,322)){ return 225; }
		else if(between(int,323,337)){ return 240; }
		else if(between(int,338,352)){ return 255; }
		else if(int >= 352){ return 270; }
		else {return null; }
	};

	let zoomed =()=>{ 
		// console.log(d3.event.transform);
		gline.attr("transform", d3.event.transform);
		ggrid.attr("transform", d3.event.transform);
		gobj.attr("transform", d3.event.transform);
		glane.attr("transform", d3.event.transform);
		gpoint.attr("transform", d3.event.transform);
		garuco.attr("transform", d3.event.transform);
		gcctv.attr("transform", d3.event.transform);
		gcar.attr("transform", d3.event.transform);
	}
	let pad = (n, width, z)=>{
	  z = z || '0';
	  n = n + '';
	  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
	}
	$scope.pad = pad;

	let gline = svg.append("g")
					.attr('class','grid');
	let gobj = svg.append("g")
					.attr('class','obj');

	let gpad = svg.append("rect")
				    .attr("width", svg.attr("width"))
				    .attr("height", svg.attr("height"))
				    .style("fill", "none")
				    .style("pointer-events", "all")
				    .call(d3.zoom()
				        .scaleExtent([1 / 20, 6])
				        .on("zoom", zoomed));

	let glane = svg.append("g")
					.attr('class','lane');
	let ggrid = svg.append("g")
					.attr('class','ggrid');
	let gpoint = svg.append("g")
					.attr('class','point');
	let garuco = svg.append("g")
					.attr('class','aruco');
	let gcctv = svg.append("g")
					.attr('class','cctv');
	let gcar = svg.append("g")
					.attr('class','car');

	// var
	$scope.maps = {};
	$scope.cars = [];
	$scope.newPoint = [];

	$scope.createGrid = (wh)=>{
		let girds = [];
		for(xi = 0; xi < wh[0]; xi++){ girds.push([xi * gridSize, 0, xi * gridSize, wh[1] * gridSize ]); }
		for(yi = 0; yi < wh[1]; yi++){ girds.push([0, yi * gridSize, wh[0] * gridSize, yi * gridSize ]); }

		gline.selectAll("line")
		    .data(girds)
		  .enter().append("line")
		    .attr("x1", d=> d[0])
		    .attr("y1", d=> d[1])
		    .attr("x2", d=> d[2])
		    .attr("y2", d=> d[3])
			.attr('stroke', '#919191')
			.attr('stroke-width', '0.4');

		girds = []; 
		for(xi = 0; xi < wh[0]; xi++){ 
			for(yi = 0; yi < wh[1]; yi++){ 
				girds.push([xi * gridSize,yi * gridSize]);
			}
		}

		// ggrid.selectAll("rect")
		//     .data(girds)
		//   .enter().append("rect")
		//   	.attr('class','on')
		//  	.attr("x", d=> d[0])
		// 	.attr("y", d=> d[1])
		// 	.attr("width", gridSize)
		// 	.attr("height", gridSize)
		// 	.attr("fill", "rgba(98, 226, 98, 1)")
		// 	.attr("opacity", 0.1)
		// 	.on("click", (d)=>{
		// 		let x = d[0]/gridSize;
		// 		let y = d[1]/gridSize;
		// 		var newid = prompt("Please enter AR number", "Add Point");
  // 				if (newid != null) {
  // 					$scope.maps.points.push({
  // 						no: parseInt(newid),
  // 						pos: [x, y],
  // 						status: 'g'
  // 					});

  // 					gpoint.append("image")
		// 			  	.attr('class','point')
		// 			  	.attr('no', parseInt(newid))
		// 			  	.attr('pos', [x, y])
		// 			  	.attr("xlink:href", "./img/point-g.png")
		// 			    .attr("x", (x * gridSize) - (gridSize / 2))
		// 				.attr("y", (y * gridSize) - (gridSize / 2))
		// 				.attr("width", gridSize * 2)
		// 				.attr("height", gridSize * 2);

		// 			$('#newPoint').val(JSON.stringify($scope.maps.points));
		// 		}
		// 	});
	}

	$scope.createObj = (obj)=>{
		gobj.selectAll("rect")
		    .data(obj)
		  .enter().append("rect")
		    .attr("x", d=> d[0] * gridSize)
			.attr("y", d=> d[1] * gridSize)
			.attr("width", d=> (d[2] - d[0]) * gridSize + gridSize)
			.attr("height", d=> (d[3] - d[1]) * gridSize + gridSize)
			.attr("fill", d=> "#2378ae");
	}

	$scope.createLane = (lane)=>{
		let lines = [];
		angular.forEach(lane, function(v, k) {
			let p1 = $scope.getPoint(v[0]);
			let p2 = $scope.getPoint(v[1]);
			lines.push([p1.pos[0], p1.pos[1], p2.pos[0], p2.pos[1]]);
		});
		glane.selectAll("line")
		    .data(lines)
		  .enter().append("line")
			.attr("x1", d=> (d[0] * gridSize) + (gridSize / 2))
			.attr("y1", d=> (d[1] * gridSize) + (gridSize / 2))
			.attr("x2", d=> (d[2] * gridSize) + (gridSize / 2))
			.attr("y2", d=> (d[3] * gridSize) + (gridSize / 2))
			.attr("stroke-width", 6)
			.attr("stroke", "rgba(98, 226, 98, 0.75)");
		// glane.selectAll("rect")
		//     .data(lane)
		//   .enter().append("rect")
		//     .attr("x", d=> d[0] * gridSize)
		// 	.attr("y", d=> d[1] * gridSize)
		// 	.attr("width", d=> (d[2] - d[0]) * gridSize + gridSize)
		// 	.attr("height", d=> (d[3] - d[1]) * gridSize + gridSize)
		// 	.attr("fill", "rgba(98, 226, 98, 0.6)");
	}

	$scope.createPoint = (point)=>{
		gpoint.selectAll("image")
		    .data(point)
		  .enter().append("image")
		  	.attr('class','point')
		  	.attr('no',d=> d.no)
		  	.attr('pos',d=> d.pos)
		  	.attr("xlink:href", d=> "./img/point-"+d.status+".png")
		    .attr("x", d=> (d.pos[0] * gridSize) - (gridSize / 2))
			.attr("y", d=> (d.pos[1] * gridSize) - (gridSize / 2))
			.attr("width", gridSize * 2)
			.attr("height", gridSize * 2)
			.on("click", (d)=>{
				alert(JSON.stringify(d));
				// if(confirm('ต้องการเรียกรถมาที่ตำแหน่งนี้ใช้หรือไม่ ' + JSON.stringify(d))){
					// $http.get('/api/order/' + d.pos[0] + '/' + d.pos[1] + '/' + d.no, ()=>{

					// });
				// }
			});
	}

	$scope.createAruco = (aruco)=>{
		garuco.selectAll("image")
		    .data(aruco)
		  .enter().append("image")
		  	.attr('class','aruco')
		  	.attr('no',d=> d.no)
		  	.attr('pos',d=> d.pos)
		  	.attr("xlink:href", d=> "./img/ar/" + d.no + ".png")
		    .attr("x", d=> (d.pos[0] * gridSize) - (gridSize / 2))
			.attr("y", d=> (d.pos[1] * gridSize) - (gridSize / 2))
			.attr("width", gridSize * 2)
			.attr("height", gridSize * 2)
			.attr("opacity", 0.4)
			.on("click", (d)=>{
				alert(JSON.stringify(d));
			});
	}

	$scope.createCctv = (cctv)=>{
		gcctv.selectAll("image")
		    .data(cctv)
		  .enter().append("image")
		  	.attr('ip', d=> d.ip)
		  	.attr('class','cctv')
		  	.attr("xlink:href","./img/camera-icon.png")
		    .attr("x", d=> (d.pos[0] * gridSize) + (7))
			.attr("y", d=> (d.pos[1] * gridSize) + (7))
			.attr("width", 16)
			.attr("height", 16)
			.on("click", (d)=>{
				console.log(d);
				var c = gcctv.select('#rect'+d.no+'');
				var attr = c.attr("display");
				gcctv.selectAll("rect")
					.attr("display", 'none');
				if(attr == 'none'){ c.attr("display", ''); }
				else{ c.attr("display", 'none'); }
			});
		gcctv.selectAll("rect")
		    .data(cctv)
		  .enter().append("rect")
		  	.attr('id', d=> 'rect'+d.no)
		  	.attr('ip', d=> d.ip)
		  	.attr('class','zcctv')
		  	.attr("display", 'none')
		    .attr("x", d=> (d.area[0] * gridSize) - (2 * gridSize))
			.attr("y", d=> (d.area[1] * gridSize) - (2 * gridSize))
			.attr("width", d=> gridSize * (d.area[2] - d.area[0]))
			.attr("height", d=> gridSize * (d.area[3] - d.area[1]))
			.attr("fill", "RGBA(216,156,86,0.2)");
	}

	$scope.car = {
		err: 7,
		create: (car)=>{
			let k = 8;//$scope.car.err;
			let num = car.ar.length > 0 ? car.ar[0][0] : 0;
			let p = $scope.getPoint(num);
			gcar.append("image")
			  	.attr('class','car')
			  	.attr('cid', car.number)
			  	.attr('pos', num)
			  	.attr("xlink:href", "./img/car/" + pad($scope.roundCompass15(car.deg),3) + '0' + (car.liftpos == 1 ? '1' : '0') + ".png")
			    .attr("x", (p.pos[0] * gridSize) - ((k/2) * gridSize) + k - 2)
				.attr("y", (p.pos[1] * gridSize) - ((k/2) * gridSize) + k - 2)
				.attr("width", k * gridSize)
				.attr("height", k * gridSize)
				.on("click", ()=>{
				});
		},
		move: (car)=>{
			var k = 8;//$scope.car.err;
			var sel = d3.selectAll('image.car[cid="'+car.number+'"').size();
			if(sel <= 0){
				$scope.car.create(car);
			}else{
				let num = car.ar.length > 0 ? car.ar[0][0] : 0;
				let p = $scope.getPoint(num);
				d3.select('image.car[cid="'+car.number+'"')
					.attr("x", (p.pos[0] * gridSize) - ((k/2) * gridSize) + k - 2)
					.attr("y", (p.pos[1] * gridSize) - ((k/2) * gridSize) + k - 2)
					.attr("xlink:href", "./img/car/" + pad($scope.roundCompass15(car.deg),3) + '0' + (car.liftpos == 1 ? '1' : '0') + ".png");
			}
		}
	}


	$scope.getPoint = (num)=>{
		let a = $scope.maps.points.find(d => d.no == num);
		if(a){
			return a;
		}else{
			return { no: 0, pos: [0,0] };
		}
	}

	$scope.socketInit = ()=>{
		$scope.socket = io.connect('ws://192.168.101.42:3001/', {query: 'room=maps'});
		if($scope.socket){
			$scope.socket.on('conn', function (msg) {
	            console.log("My id: " + msg);
	        });

	        $scope.socket.on('var', function (msg) {
	        	$scope.var = msg;
	            $scope.car.move($scope.var);
	        });
	    }
		// $scope.socket = io.connect('/', {query: 'room=maps'});
		// if($scope.socket){
	 //        $scope.socket.on('conn', function (msg) {
	 //            console.log("Socket connect " + msg);
	 //        });
	 //        $scope.socket.on('move', function (d) {
	 //            console.log("Socket move " + d);
		// 		let k = $scope.car.err;
		// 		$scope.car.move(d);
	 //        });

	 //        $scope.socket.on('carsUpdate', function (d) {
	 //        	$scope.$apply(function(){ $scope.cars = d; });

	 //        	for(i = 0; i < $scope.cars.length; i++){
	 //        		$scope.car.move($scope.cars[i]);
	 //        	}
	        	
	 //            console.log("Socket cars update " + d);
	 //        });
	 //    }
	}

	$http.get('/api/data/maps/3').then((res)=>{
		if(res.data.err){ console.error(res.data.err); return; }
		$scope.maps = res.data.doc;

		$scope.createGrid($scope.maps.size);
		$scope.createObj($scope.maps.obj);
		$scope.createLane($scope.maps.lane);
		$scope.createPoint($scope.maps.points);
		$scope.createAruco($scope.maps.points);
		$scope.createCctv($scope.maps.cctv);

		$scope.socketInit();

		console.dir($scope.maps);
	}, (err)=>{
		console.error(err);
	});
});