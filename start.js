var pm2 = require('pm2');
const SerialPort = require('serialport');

SerialPort.list(function( err, results ) {
  	console.log(results);

	let arduino = results.find(d => d.vendorId == '1a86');
	let rplidar = results.find(d => d.vendorId == '10c4');
	let a = r = 'null';
	if(arduino){ a = arduino.comName; }
	if(rplidar){ r = rplidar.comName; }

	pm2.connect(function(err) {
	  if (err) {
	    console.error(err);
	    process.exit(2);
	  }
	  	setTimeout(()=>{
			pm2.start({
				script: 'main.js',
				args: ['--arduino', a, '--rplidar', r],
				interpreter: "node"
			}, function(err, apps) {
				console.log(apps);

				pm2.disconnect(); 
				if (err) throw err
			});
		}, 3000);
	});
});