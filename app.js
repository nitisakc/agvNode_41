const express = require('express');
const path = require('path');
// const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const http = require('http');
const pjson = require('./package.json');

global.arcount = 0;
global.syss = pjson.syss;
global.var = pjson.var;
global.zone = [99, 99, 99];

// var ArgumentParser = require('argparse').ArgumentParser;
// var parser = new ArgumentParser();
// parser.addArgument([ '-p', '--port' ], { defaultValue: pjson.port, required: false, type: 'string' });
// var args = parser.parseArgs();

const index = require('./routes/index');
const ar = require('./routes/ar');
const safety = require('./routes/safety');
const to = require('./routes/to');
const config = require('./routes/config');

let app = express();

// app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false, limit: '10mb' }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'www')));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use('/', index);
app.use('/ar', ar);
app.use('/safety', safety);
app.use('/to', to);
app.use('/config', config);

const port = pjson.port;
app.set('port', port);
var server = http.createServer(app);
server.listen(port);

// global.log = (msg, type = "log")=>{
//  global.var.logs.unshift({ msg: msg, type: type, time: new Date() });
//  if(global.var.logs.length > 20) global.var.logs.pop();
// }
global.logs = [];
let old;
global.log = (msgs)=>{
  if(old != msgs){
    old = msgs;
    let dd = new Date();//dd.getHours()+''+
    global.logs.unshift(dd.getTime() + '|' + msgs);
    console.log(msgs);

    if(global.io){ global.io.emit('logs', global.logs); }

    if(global.logs.length > 20){
      global.logs.pop();
    }
  }
}

global.io = require('socket.io').listen(server);

global.tcsio = require('socket.io-client')
        .connect(pjson.tcshost, { query: `room=cars&number=${pjson.carNumber}&type=${pjson.carType}&port=${pjson.port}` });
// global.tcsio.on('connect', () => { global.lamp.w.strobe(500); });
global.tcsio.on('zone', (data) => { 
  global.zone = data;
});

global.tcsio.on('buffwipin', (data) => { 
  global.var.buffer.wipin = data == -1 ? null : data;
});

global.tcsio.on('buffpumpin', (data) => { 
  global.var.buffer.pumpin = data == -1 ? null : data;
});

setInterval(()=>{
  // console.dir(global.var);
  global.tcsio.emit('var', global.var);
}, 500);

module.exports = app;