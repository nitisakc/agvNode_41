var express = require('express');
var router = express.Router();

router.post('/set', function(req, res, next) {
	// console.log(req.body);
	let body = req.body;
	if(body.length == 0){
		global.arcount++;
	}
	if(body.length > 0 || global.arcount > 10){
		global.var.ar = body;
		global.var.ar.sort((a,b)=>{
			if (a[1] > b[1]) {
		        return 1;
		    }
		    if (b[1] > a[1]) {
		        return -1;
		    }
		    return 0;
		});
	}

	res.sendStatus(200);
});

module.exports = router;
