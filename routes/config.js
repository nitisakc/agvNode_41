var express = require('express');
var router = express.Router();

const fs = require('fs');

router.get('/getconfig/:name', function(req, res, next) {
    let rawdata = fs.readFileSync(`./config/${req.params.name}.json`);
    res.send(JSON.parse(rawdata));
});

router.post('/save/:name', function(req, res, next) { 
    console.log(req.body); 
    fs.copyFile(`./config/${req.params.name}.json`, `./config/temp/${req.params.name}-${new Date().getTime()}.json`,()=>{
        fs.writeFileSync(`./config/${req.params.name}.json`, JSON.stringify(req.body, null, 2));
        res.sendStatus(200);
    });
});

module.exports = router;
