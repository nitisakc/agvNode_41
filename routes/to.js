var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.sendfile('www/to.html');
});

router.get('/ready/:flag', function(req, res, next) {
	global.var.ready = req.params.flag == 0 ? false : true;
	res.send(global.var.ready);
});


router.get('/steps', function(req, res, next) {
  res.send(require('../steps.json'));
});

router.get('/set/:id/:buf', function(req, res, next) {
	// console.log(req.body);
	global.var.to = req.params.id;
	global.var.buffer = req.params.buf;

	res.send(200);
});

router.get('/setbuf/:buf', function(req, res, next) {
	// console.log("buf",req.params.buf);
	global.var.buffer = req.params.buf == -1 ? null : req.params.buf;

	res.send(200);
});


router.get('/set/:id', function(req, res, next) {
	// console.log(req.body);
	if(global.var.to == null && global.var.ready){
		global.var.to = req.params.id;
		res.send(200);
	}else{
		res.send(404);
	}
});

router.get('/zone/:zone', function(req, res, next) {
	// console.log(req.body);
	let z = req.params.zone;
	z = z.split(',');
	if(z.length == 3){
		global.zone = (z == -1 ? null : z);
	}
	res.send(global.zone);
});

router.get('/zonenull/:zone', function(req, res, next) {
	global.zone[req.params.zone] = null;
	res.send(global.zone);
});
router.get('/zonenull', function(req, res, next) {
	global.zone = [null, null, null];
	res.send(global.zone);
});
router.get('/zone', function(req, res, next) {
	res.send(global.zone);
});
module.exports = router;
