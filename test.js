//[180, 860]
const five = require("johnny-five");

const board = new five.Board({ repl: false, debug: true, port: '/dev/ttyUSB1' }); //


board.on("ready", ()=> {
	let lamp = {
		r: new five.Led({ pin: 34 }),
		o: new five.Led({ pin: 35 }),
		g: new five.Led({ pin: 36 }),
		b: new five.Led({ pin: 37 }),
		w: new five.Led({ pin: 38 })
	};

	lamp.r.off();
	lamp.o.off();
	lamp.g.off();
	lamp.b.off();
	lamp.w.off();

	setTimeout(()=>{
		lamp.r.on();
		lamp.o.off();
		lamp.g.off();
		lamp.b.off();
		lamp.w.off();
	}, 1000);

	setTimeout(()=>{
		lamp.r.off();
		lamp.o.on();
		lamp.g.off();
		lamp.b.off();
		lamp.w.off();
	}, 2000);

	setTimeout(()=>{
		lamp.r.off();
		lamp.o.off();
		lamp.g.on();
		lamp.b.off();
		lamp.w.off();
	}, 3000);

	setTimeout(()=>{
		lamp.r.off();
		lamp.o.off();
		lamp.g.off();
		lamp.b.on();
		lamp.w.off();
	}, 4000);

	setTimeout(()=>{
		lamp.r.off();
		lamp.o.off();
		lamp.g.off();
		lamp.b.off();
		lamp.w.on();
	}, 5000);

});