var ArgumentParser = require('argparse').ArgumentParser;
var parser = new ArgumentParser();
parser.addArgument([ '-a', '--arduino' ], { defaultValue:"/dev/ttyUSB1", required: false, type: 'string' });
parser.addArgument([ '-r', '--rplidar' ], { defaultValue:"/dev/ttyUSB0", required: false, type: 'string' });
var args = parser.parseArgs();

console.log("arduino "+args.arduino);
console.log("rplidar "+args.rplidar);

global.args = args;

let app = require('./app');
const d3 = require("d3-scale");
const fs = require('fs');
const calc = require('./utils/calc');
const func = require('./utils/func');
const { board, relay, lamp, move, lift, other } = require('./arduino');
// const syss = require('./syss');
let schedule = require('node-schedule');
let conf = require('./config/main.json');
let steps = require('./config/steps.json');
const request = require('request');

app.get('/getconfig/:name', function(req, res, next) {
    let rawdata = fs.readFileSync(`./config/${req.params.name}.json`);
    res.send(JSON.parse(rawdata));
});
app.post('/save/:name', function(req, res, next) { 
    console.log(req.body); 
    fs.copyFile(`./config/${req.params.name}.json`, `./config/temp/${req.params.name}-${new Date().getTime()}.json`,()=>{
        fs.writeFileSync(`./config/${req.params.name}.json`, JSON.stringify(req.body, null, 2));
        res.sendStatus(200);
    });
});
app.post('/savestep/:loc', function(req, res, next) {
    // console.log(req.body); 
    let rawdata = fs.readFileSync('./config/steps.json');
    rawdata = JSON.parse(rawdata);
	// console.log(rawdata[req.params.loc]);
    rawdata[req.params.loc] = req.body;
    fs.copyFile('./config/steps.json', `./config/temp/steps-${new Date().getTime()}.json`,()=>{
        fs.writeFileSync('./config/steps.json', JSON.stringify(rawdata, null, 2));
		steps = req.body;
		console.log(steps);
        res.sendStatus(200);
    });
});

// global.zone = [99,99,99];

let calcErr = d3.scaleLinear().domain([0, 180]).range([180, 0]).clamp(true);

let fixSpeed = conf.fixSpeed;
let palletpoint = conf.palletpoint;
let nonSafety = conf.nonSafety;
let xoffset = conf.xoffset;
let doffset = conf.doffset;

let runInter, offsetInter, turnInter, bufInter,
	waitTime, jobTime;

let degNow = 180, 
	sef = global.var.safety.on, 
	backoffset = conf.backoffset, 
	osltmp = conf.oslRun,
	l = 1, 
	step = 0,
	lflag = true;

var j = schedule.scheduleJob('0 2,8,14,20 * * *', ()=>{
	other.won();
	global.var.changebatt = false;
	global.log('Please change the battery');
});

process.on('unhandledRejection', (reason, promise) => {
  console.log('Unhandled Rejection at:', reason.stack || reason)
})

let clearjob = ()=>{
	global.log('Clear Job.');
	clearInterval(runInter);
	clearInterval(offsetInter);
	clearInterval(turnInter);
	global.var.route = [];
	global.var.to = null;
	global.var.buffer = { wipin: null, pumpin: null };
	move.stop();
}

let goHome = (num)=>{
	if(global.var.ar.length > 0 && num){
		if(waitTime){ clearTimeout(waitTime); waitTime = null; }
		if(jobTime){ clearTimeout(jobTime); jobTime = null; }
		if(num == 51){
			global.log('Go to home. At standby.'); 
			global.var.ready = false;
			global.var.to = 0;
			turn(180, ()=>{
				global.var.route = [6 ,40, 42, 39, 44, 49, 53, 45, 36, 122, 139]; 
				// global.var.route = [6 ,40, 42, 39, 4, 49, 5, 45, 36, 43, 37, 21, 24, 35, 23, 12, 33, 25, 26, 38];
				run(true, ()=>{
					turn(0, ()=>{
						// global.var.route = [8];
						// run(true, ()=>{
							global.var.to = null;
							global.var.buffer = { wipin: null, pumpin: null };
							// seeJob();
						// });
					}, true);
				});
			}, true);
		}else if(num == 111){
			global.log('Go to home. At wait zone.'); 
			global.var.ready = false;
			global.var.to = 0;
			turn(180, ()=>{
				global.var.route = [122, 139];
				// global.var.route = [49, 5, 45, 36, 43, 37, 21, 24, 35, 23, 12, 33, 25, 26, 38];
				run(true, ()=>{
					turn(0, ()=>{
						// global.var.route = [8];
						// run(true, ()=>{
							global.var.to = null;
							global.var.buffer = { wipin: null, pumpin: null };
							// seeJob();
						// });
					}, true);
				});
			}, true, true);
		}
	}
}

let toWaitZone = ()=>{
	if(global.var.ar.length > 0){
		// global.var.ready = false();
		global.log('Go to waiting zone.');
		// global.var.route = [25, 33, 12, 23, 35, 24, 21, 37, 43, 36, 45 ,5, 100, 107, 111];
		global.var.route = [{ num: 122, zone: 1 }, 123, 36, 316, 317, 318];
		run(true, ()=>{
			global.var.to = 0;
			global.var.buffer = { wipin: null, pumpin: null };
			if(global.zone[0] == null){
				toStandbyFromWaitZone();
			}else{
				waitZone();
			}
		}, 100);
		// global.var.route = [122, 123, 36];
		// run(true, ()=>{
		// 	global.var.to = null;
		// 	global.var.buffer = { wipin: null, pumpin: null };
		// 	if(global.var.ready == true){
		// 		turn(180, ()=>{
		// 			waitJob();
		// 		},false);
		// 	}
		// });
	}
}
let toWaitZoneFromPumpIn = ()=>{
	if(global.var.ar.length > 0){
		// global.var.ready = false();
		global.log('Go to waiting zone.');
		// global.var.route = [25, 33, 12, 23, 35, 24, 21, 37, 43, 36, 45 ,5, 100, 107, 111];
		global.var.route = [275, 274, 273, 272, 271, 270, 269, 268, 267, 266, 265, 264, 263, 262, 261, 260, { num: 37, zone: 1 }, 43, 36, 316, 317, 318];
		run(true, ()=>{
			global.var.to = 0;
			global.var.buffer = { wipin: null, pumpin: null };
			if(global.zone[0] == null){
				toStandbyFromWaitZone();
			}else{
				waitZone();
			}
		}, 100);
	}
}

let toStandbyFromWaitZone = ()=>{
	if(global.var.ar.length > 0){
		global.log('Go to standby.');
		turn(60, ()=>{
			// global.var.route = [39, 42, 31, 40, 54, 50, 51];
			// global.var.route = [25, 33, 12, 23, 35, 24, 21, 37, 43, 36, 45, 5, 53, 49, 44, 4, 39, 42, 31, 40, 54, 50, 51];
			// global.var.route = [5, 53, 49, 44, 4, 107, 42, 31, 40, 54, 50, 51];
			global.var.route = [5, 53, 49, 44, 102, 107, 42, 31, 40, 54, 50, 51];
			run(true, ()=>{
				global.var.to = null;
				global.var.buffer = { wipin: null, pumpin: null };
				if(global.var.ready == true){
					waitJob();
				}
			});
		}, true);
	}
}

let toStandby = ()=>{
	if(global.var.ar.length > 0){
		global.log('Go to standby.');
		// turn(0, ()=>{
			// global.var.route = [39, 42, 31, 40, 54, 50, 51];
			// global.var.route = [25, 33, 12, 23, 35, 24, 21, 37, 43, 36, 45, 5, 53, 49, 44, 4, 39, 42, 31, 40, 54, 50, 51];
			// global.var.route = [122, 123, 36, 45, 5, 53, 49, 44, 4, 107, 42, 31, 40, 54, 50, 51];
			global.var.route = [122, 123, 36, 45, 5, 53, 49, 44, 102, 107, 42, 31, 40, 54, 50, 51];
			run(true, ()=>{
				global.var.to = null;
				global.var.buffer = { wipin: null, pumpin: null };
				if(global.var.ready == true){
					waitJob();
				}
			});
			
		// }, false);
	}
}
// [31, 42, 32, { num: 20, zone: 2 }, 44, 49, 5];

let waitZone = ()=>{
	// falseRun();
	global.log('Waiting for free standby zone.');
	waitTime = setTimeout(()=>{
		if(global.zone[0] == null){
			toStandbyFromWaitZone();
		}else{
			waitZone();
		}
	}, 3000);
}

let waitJob = ()=>{
	jobTime = setTimeout(()=>{
		// if(global.var.ar.length > 0 && global.var.ar[0][0] == 51 && global.var.ar[0][1] < 40){
			global.log('Standby. Waiting for job.');
			request({
			    method: 'GET',
			    url: `http://192.168.108.7:3310/api/waitjob/${global.var.number}`,
			    timeout: 1000
			}, (err, res, body)=>{
				if(err){
					global.log('Send waitjob error.');
				}
			});

			if(global.var.to == 0){

			}
			else if(global.var.to != null && global.var.ready){
				step = 0;
				if(steps[global.var.to]){
					global.log(`Get a job. Go to ${global.var.to}`);
					loop(steps[global.var.to]);
				}
				// console.dir(steps[global.var.to]);
			}else{
				waitJob();
			}
		// }else{
		// 	waitJob();
		// }
	}, 3000);
}

global.io.on('connection', function(socket) {
	global.io.to(socket.id).emit('conn', socket.id);
	global.io.emit('logs', global.logs);

	socket.on('ar', 	 (msgs)=> { global.var.ar 		= msgs; });

	socket.on('pidval',  (msgs)=> { 
		global.var.pidval = msgs; 
	    global.io.emit('pidval', msgs);
	});

	socket.on('img',  (msgs)=> { console.log(msgs); });

	socket.on('selDeg',  (msgs)=> { global.var.selDeg = msgs; });
	socket.on('selSpd',  (msgs)=> { global.var.selSpd = msgs; });
	socket.on('en',  (msgs)=> { move.en(msgs); });
	socket.on('beep',  (msgs)=> { if(msgs) other.beep(); });
	socket.on('pidon',  (msgs)=> { global.var.pidon = msgs; });
	socket.on('dir',  (msgs)=> { msgs == 0 ? move.stop() : move.dir(msgs == 1); });
	socket.on('liftup',  (msgs)=> { lift.process(msgs); });
	socket.on('goHome',  (msgs)=> { goHome(msgs); });
	socket.on('changeBatt',  (msgs)=> { 
			// toBufferTest();
			// testCam();
    	other.woff();
    	global.var.changebatt = true;
		global.log('Battery changed.'); 
	});
	socket.on('loadConfig',  (msgs)=> {
		let rawdata = fs.readFileSync('./config/main.json');
		conf = JSON.parse(rawdata);
		rawdata = fs.readFileSync('./config/steps.json');
		steps = JSON.parse(rawdata);

		fixSpeed = conf.fixSpeed;
		palletpoint = conf.palletpoint;
		nonSafety = conf.nonSafety;
		xoffset = conf.xoffset;
		doffset = conf.doffset;
		backoffset = conf.backoffset;
		osltmp = conf.oslRun;

		console.log('Reload main config.')
		console.dir(conf);
		console.log('Reload steps.')
		console.dir(steps);
	});
	socket.on('toStandby',  (msgs)=> { 
		// toStandby();  
		// testCam();
		//toWaitZone();
		// if(msgs == 51){
			toStandby(); 
		// }else{
		// 	toWaitZone(); 
		// }
	});
	socket.on('clearjob',  (msgs)=> { clearjob(); });
	socket.on('clearCurrentJob',  (msgs)=> { clearCurrentJob(); });
	socket.on('safety',  (msgs)=> { 
		sef = !sef;
		global.var.safety.on = sef;
	});
	socket.on('seejob',  (msgs)=> { 
		step = 0;
		// global.var.ready = true;
		// waitJob(); 
		// toWaitZone();

		// testCam();
		if(msgs == 51){
			global.var.ready = true; 
			waitJob(); 
		}else if(msgs == 318){
			global.var.ready = true; 
			waitZone();
		}
	});
});

setInterval(()=>{
    global.io.emit('var', global.var);
}, 400);

let ddd  = -1;
let toBufferWipIn_5x2 = (callback)=>{
	// ddd++;
	global.log("Call");
	if(global.var.to != 0 && global.var.to != null){ 
		clearOrder(global.var.to, ()=>{});
	}
	// turn(0, ()=>{
		bufInter = setInterval(()=>{
			// global.var.buffer = ddd;
			// global.var.buffer.wipin = 4;
			if(global.var.buffer.wipin != null){ 
				global.log("clearInterval");
				clearInterval(bufInter);
				let b = global.var.buffer.wipin ;
				let bosl = conf.bufferosl;
				let rr = []; 
				if(b > 4){
					global.var.route = [127,141];
					run(false, ()=>{
						// turn(0, ()=>{
							if(b == 9){ 
								bosl = -380;
								global.var.route = [160];
								rr = [139]; 
							}else if(b == 8){ 
								bosl = -300;
								global.var.route = [160,24];
								rr = [127,139]; 
								// rr = [160,141,139]; 
							}else if(b == 7){ 
								bosl = -295;
								global.var.route = [160,169];
								rr = [141,139]; 
							}else if(b == 6){ 
								bosl = -315;
								global.var.route = [160,169,173];
								rr = [160,141,139]; 
							}else if(b == 5){ 
								bosl = -360;
								global.var.route = [160,169,173,74];
								rr = [169,160,141,139]; 
							}

							if(global.var.liftpos != 1){
								lift.process(1, ()=>{ });
							}
							global.log(`Go to buffer index ${global.var.buffer.wipin}.`);
							
							run(false, ()=>{
								lift.process(2, ()=>{
									global.var.route = rr;
									run(true, ()=>{
										global.log("Done");
										
										callback(); 
										// toBufferWipIn();
									});
								});
							}, bosl);
						// },true);
					}, conf.bufferosl);
				}else{
					if(b == 4){ 
						bosl = -280;//-240;
						global.var.route = [137, 128];
						rr = [139 ,21]; }
					else if(b == 3){ 
						bosl = -285;//-265;
						global.var.route = [137, 128, 120];
						rr = [137, 139 ,21]; }
					else if(b == 2){ 
						bosl = -335; //-315;
						global.var.route = [137, 128, 120, 126];
						rr = [128, 137, 139 ,21]; }
					else if(b == 1){ 
						bosl = -355; //-310
						global.var.route = [137, 128, 120, 126, 133];
						rr = [120, 128, 137, 139 ,21]; }
					else if(b == 0){ 
						bosl = -380;//-360;
						global.var.route = [137, 128, 120, 126, 133, 136];
						rr = [126, 120, 128, 137, 139 ,21]; }

					if(global.var.liftpos != 1){
						lift.process(1, ()=>{ });
					}
					global.log(`Go to buffer index ${global.var.buffer.wipin}.`);
					// global.var.buffer++;
					if(global.var.buffer.wipin+1 > 4){global.var.buffer.wipin=0;}
					run(false, ()=>{
						lift.process(2, ()=>{
							global.var.route = rr;
							run(true, ()=>{
								global.log("Done");
								
								callback(); 
								// toBufferWipIn();
							});
						});
					}, bosl);
				}
			}else{
				global.log('Buffer is NULL');
			}
		}, 1000);
	// }, true, true);
}
let toBufferPumpIn = (callback)=>{
	if(global.var.to != 0 && global.var.to != null){ 
		clearOrder(global.var.to, ()=>{});
	}
	bufInter = setInterval(()=>{
		if(global.var.buffer.pumpin != null){ 
			global.log("clearInterval");
			clearInterval(bufInter);
			let b = global.var.buffer.pumpin ;
			let bosl = conf.bufferosl;
			let rr = []; 

			if(b == 2){ 
				bosl = -340;
				global.var.route = [279, 168];
				rr = [277]; 
			}else if(b == 1){ 
				bosl = -260;
				global.var.route = [279, 281];
				rr = [119, 277]; 
				// rr = [160,141,139]; 
			}else if(b == 0){ 
				bosl = -330;
				global.var.route = [279, 281, 283];
				rr = [279, 119, 277]; 
			}

			turn(90, ()=>{
				run(false, ()=>{
					lift.process(2, ()=>{
						global.var.route = rr;
						run(true, ()=>{
							turn(0, ()=>{
								global.log("Done");
								callback(); 
							}, false);
						});
					});
				},bosl);
			}, false);
		}
	}, 1000);
}

let toBufferTest = (callback)=>{
	
	global.var.route = [127,141];
	run(false, ()=>{
		bosl = -390;
		global.var.route = [160];
		// bosl = -310;
		// global.var.route = [160,24];
		// bosl = -290;
		// global.var.route = [160,169];
		// bosl = -325;
		// global.var.route = [160,169,173];
		// bosl = -360;
		// global.var.route = [160,169,173,74];
		turn(0, ()=>{
			run(false, ()=>{
				global.var.route = [139]//[169,160,141,139];
				run(true, ()=>{
					
						// toStandby(); 
				});
			},bosl);
		},true);
	}, conf.bufferosl);
}

let loop = (s)=>{
	if(s){
		if(step < s.length){
			let inx = step;
			global.log(`Loop ${step} : ${s[inx].event}`);

			if(s[inx].event == 'run'){
				global.var.route = JSON.parse(JSON.stringify(s[inx].route));
				if(s[inx].osl){
					run(s[inx].dir, ()=>{ step++; loop(s); }, s[inx].osl);
				}else{
					run(s[inx].dir, ()=>{ step++; loop(s); });	
				}
			}else if(s[inx].event == 'turn'){
				turn(s[inx].deg, ()=>{ step++; loop(s); }, s[inx].dir);
			}else if(s[inx].event == 'lift'){
				//loop(s);
				lift.process(s[inx].dir, ()=>{ step++; loop(s); });
			}else if(s[inx].event == 'clearorder'){
				//
				// clearOrder(global.var.to, ()=>{ step++; loop(s); });
				clearOrder(global.var.to, ()=>{
					// global.var.to = null;
					step++;
					loop(s);
				});
			}else if(s[inx].event == 'toBufferWipIn'){
				toBufferWipIn_5x2(()=>{
				// toBufferWipIn(()=>{
					// toStandby();
					toWaitZone();
				})
			}else if(s[inx].event == 'toBufferPumpIn'){
				toBufferPumpIn(()=>{
                    toWaitZoneFromPumpIn();
				})
			}
		}
		// else{
		// 	toBufferWipIn(()=>{
		// 		toStandby();
		// 		// toWaitZone();
		// 	});
		// }
	}
}

var pm2 = require('pm2');

pm2.connect(function(err) {
  	if (err) {
	    console.error(err);
	    process.exit(2);
  	}
	pm2.start({
    	script    : 'py/ar.py',         // Script to be run
    	interpreter: 'python3',
    	args: ['--back', backoffset]
	}, function(err, apps) {
//  		pm2.start({
//		    script    : 'py/safety.py',         // Script to be run
//		    interpreter: 'python3',
  //  		args: ['--port', args.rplidar]
//		}, function(err, apps) {
//		    pm2.disconnect();   // Disconnects from PM2
//		    if (err) throw err
//		});
 	});
});

let clearOrder = (t, callback)=>{
    other.bon();
	request.get({
	    method: 'GET',
	    url: `http://192.168.108.7:3310/api/clrorder/${t}`,
	    timeout: 3000
	}, (err, res, body)=>{
    		other.boff();
	    	callback();
	    }
	);
}

let run = (dir = true, callback, osl = osltmp)=>{
	global.log('Routing ' + JSON.stringify(global.var.route));
	let ar0count = 0, lencount = 0;
	runInter = setInterval(()=>{
		if(global.var.route.length == 0){
			clearInterval(runInter);
			move.stop();
			global.log('Route 0');
			if(callback){ callback(); }
		}else if(global.var.route.length > 0){
			let block = null;
			if(typeof global.var.route[0] === 'object'){
				block = global.var.route[0];
			}
			let num =  block ? block.num : global.var.route[0];
			block = block && (global.zone[block.zone] == null || global.zone[block.zone] == global.var.number) ? null : block; //***Check Zone***//

			if(nonSafety.indexOf(num) > -1){
				global.var.safety.on = false;
			}else{
				global.var.safety.on = sef;
			}

			let a = global.var.ar.find(d => d[0] == num);
			if(a){
				let t = block ? ' and waiting free buffer zone.' : '';
				global.log(`Go to number  ${num}${t}`);
				if(dir){
					let dos = 0; 
					let d = doffset.find(d => d.num == num);
					if(d){ 
						dos = parseInt(180 - Math.abs(Math.atan2(a[4], a[3] + d.offset) * 180 / Math.PI));
					}else{
						dos = a[5];
					}
					// if(global.var.route.length > 1){ osl = osltmp; }
					if(num == 111){ osl = 180; }
					if(a[6] == 'F' && a[4] > osl){
						global.var.selDeg = dos;
						global.var.pidval = dos;
						if(global.var.route.length > 1 && a[4] < conf.nextoffset && block == null){
							global.var.route.shift();
						}
					}else{
						if(block != null){
							block.stop = true;
						}else{
							global.var.route.shift();
						}
					}
				}else{
					let bs = (osl != osltmp ? osl : (backoffset * -1));
					let xos = 0; 
					let x = xoffset.find(d => d.num == num);
					if(x){ xos = x.offset; }
					if(a[4] > bs){
						global.var.route.shift();
					}else if(palletpoint.indexOf(num) > -1 && (global.var.rds[1] < -1 || global.var.rds[0] < -1)){
						lencount++;
						if(lencount > 3){
							global.log('rds = ' + global.var.rds[0] + ',' + global.var.rds[1]);
							global.var.route = [];
						}else{
							global.var.selDeg = parseInt(90 + ((xos + a[3]) / 1));
							global.var.pidval = parseInt(90 + ((xos + a[3]) / 1));
						}
					}else{
						lencount = 0;
						global.var.selDeg = parseInt(90 + ((xos + a[3]) / 1));
						global.var.pidval = parseInt(90 + ((xos + a[3]) / 1));
					} 
				}
				
				if(global.var.route.length == 0 || (block && block.stop)){ 
					move.stop(); 
				}
				else{
					if(global.var.route.length == 1 || block){
						if(fixSpeed.indexOf(num) > -1){ move.run(dir, (dir ? conf.fixSpeedVal[0] : conf.fixSpeedVal[1]), true); }
						else{
							move.run(dir, (Math.abs(a[4]) < conf.last.len) ? conf.last.spd[0] : conf.last.spd[1], true);
						}
					}else{
						if(fixSpeed.indexOf(num) > -1){ move.run(dir, (dir ? conf.fixSpeedVal[0] : conf.fixSpeedVal[1]), true); }
						else{ move.run(dir, (dir ? conf.runspd[0] : conf.runspd[1]), true); }
					}
				}
				ar0count = 0;
			}else if(block == null){
				if(ar0count > conf.ar0count){
					move.stop();
					global.var.selDeg = 90;
					global.log(`ไม่พบป้ายเบอร์ ${num} . โปรดเคลื่อนย้ายไปบริเวณป้ายเบอร์ ${num}.`);
				}else{
					move.run(dir, (dir ? conf.ar0spd[0] : conf.ar0spd[1]), false);
					ar0count = ar0count + 1;
				}
			}
		}
	}, 20);
}

let offset = (no, callblack)=> {
	global.var.selDeg = 90;
	global.log('Run Offset ' + no);
	offsetInter = setInterval(()=>{
		let a = global.var.ar.find(d => d[0] == no);
		if(a){
			global.var.selDeg = 90 + (a[2] - degNow);
		}else if(global.var.ar.length > 0){
			global.var.selDeg = 90 + (global.var.ar[0][2] - degNow);
		}

		if(a && (global.var.currDeg > 85 || global.var.currDeg < 95)){
			let l = a[4];
			if(l > -200){
				move.run(true, 50, false);
			}else if(l < -140){
				move.run(false, 50, false);
			}else{
				move.stop();
				clearInterval(offsetInter);
				global.log('Offset ' + no + ' done.');
				if(callblack){ callblack(); }
			}
		}
	}, 20);
}

let turn = (d, callblack, rd = null, lowspd = false)=>{
	global.log('Turning ' + d);
	let turnFlag = true;
	turnInter = setInterval(()=>{
		if(global.var.ar.length > 0){
			let z = (rd == true && d == 0 ? 359 : d);
			// z = (z == 180 ? 170 : z);
			let currDeg = global.var.ar[0][2];
			currDeg = currDeg < 0 ? 360 + currDeg : currDeg;
			let diff = currDeg - z; 

			let dir = z - calc.absDeg(currDeg);
			let tr = 180, tl = 0;

			if(Math.abs(diff) > (lowspd ? 5 : 2)){
				if(dir >= 0 && turnFlag){ 
    				global.var.selDeg = tr;
				}
				else if(dir < 0 && turnFlag){ 
    				global.var.selDeg = tl;
				}

				if(rd != null){
					global.var.selDeg = rd ? tr : tl;
				}
				
				if((global.var.selDeg == tr && (rd == true ? global.var.currDeg < 349 : (global.var.currDeg > (tr - (lowspd ? 10 : 5))))) || (global.var.selDeg == tl && global.var.currDeg < (tl + (lowspd ? 10 : 5)))){
					turnFlag = false;
					if(Math.abs(diff) > (lowspd ? conf.turnDeg[0] : conf.turnDeg[1])){
						move.run(true, (lowspd ? conf.turnspd[0] : conf.turnspd[1]));
					}else{
						move.run(true, (lowspd ? conf.turnspdrdu[0] : conf.turnspdrdu[1]));
					}
				}
			}else{					
				// global.log('AR 0');
    			global.var.selDeg = 90;
    			move.stop();
				if(global.var.currDeg < 92 || global.var.currDeg > 88){
					clearInterval(turnInter);
					func.wait(200, ()=>{
						global.log('Turn ' + d );
						callblack();
					});
				}
			}
		}
	}, 10);
}

let trueRun = ()=>{
	global.var.route = [13, 8, 38]; 
	run(true, ()=>{
		func.wait(2000, ()=>{
			falseRun();
		});
	});
}
let falseRun = ()=>{
	global.var.route = [8, 13, 7, 10, 14]; 
	run(false, ()=>{
		func.wait(2000, ()=>{
			trueRun();
		});
	});
}

setTimeout(()=>{
}, 3000);

let testCam = ()=>{
	loop([{
      "event": "toBufferWipIn"
    }]);
	// global.var.route = [47,105,7,8,10,13,14];
	// run(true, ()=>{

	// },60);

	// turn(270, ()=>{
	// 	global.var.route = [9];
	// 	run(false, ()=>{

	// 	});
	// },false);
	// loop(
	// 	[{ "event": "run", "dir": true, "route": [47,105,7,8,10,13,14,18], "osl": 195 },
	// 	{ "event": "turn", "deg": 270, "dir": false },
	// 	{ "event": "run", "dir": false, "route": [11], "osl": -295 }]
	// );
	// loop(
	// 	[{ "event": "run", "dir": true, "route": [47,105,7,8,10,13,14], "osl": 270 },
	// 	{ "event": "turn", "deg": 270, "dir": false },
	// 	{ "event": "run", "dir": false, "route": [15], "osl": -295 }]
	// );
	// loop(
	// 	[{ "event": "run", "dir": true, "route": [47,62,7,8,10,13,14], "osl": 420 },
	// 	{ "event": "turn", "deg": 270, "dir": false },
	// 	{ "event": "run", "dir": false, "route": [19], "osl": -285 }]
	// );
	// loop(
	// 	[{ "event": "run", "dir": true, "route": [47,62,7,8,10,13], "osl": 245 },
	// 	{ "event": "turn", "deg": 270, "dir": false },
	// 	{ "event": "run", "dir": false, "route": [9], "osl": -285 }]
	// );

	// loop(
	// 	[{ "event": "run", "dir": true, "route": [260, 261, 262, 263, 264, 265, 266, 267, 268, 269, 270, 271, 272, 273, 274, 275, 79, 117], "osl": 1 },
	// 	{ "event": "turn", "deg": 90, "dir": false },
	// 	{ "event": "run", "dir": false, "route": [279, 281, 283], "osl": -330 }]
	// );
	// loop(
	// 	[{ "event": "run", "dir": true, "route": [274, 275, 79, 117], "osl": 1 },
	// 	{ "event": "turn", "deg": 90, "dir": false },
	// 	{ "event": "run", "dir": false, "route": [279, 281], "osl": -260 }]
	// );
	// loop(
	// 	[{ "event": "run", "dir": true, "route": [274, 275, 79, 117], "osl": 1 },
	// 	{ "event": "turn", "deg": 90, "dir": false },
	// 	{ "event": "run", "dir": false, "route": [279, 168], "osl": -340 }]
	// );

	// loop(
	// 	[{ "event": "run", "dir": true, "route": [279, 119, 277] },
	// 	{ "event": "turn", "deg": 0, "dir": false },
	// 	{ "event": "run", "dir": true, "route": [275, 274]}]
	// );
	// loop(
	// 	[{ "event": "run", "dir": true, "route": [119, 277] },
	// 	{ "event": "turn", "deg": 0, "dir": false },
	// 	{ "event": "run", "dir": true, "route": [275, 274]}]
	// );
	// loop(
	// 	[{ "event": "run", "dir": true, "route": [277] },
	// 	{ "event": "turn", "deg": 0, "dir": false },
	// 	{ "event": "run", "dir": true, "route": [275, 274]}]
	// );
//[270,271,272,273,274,275,276,277,278,152,280,281,282,283,284,79,81,94,37,69,124,125,131,132,161,106,111,89,121,112,117] }]
// { "event": "run", "dir": true, "route": [278,152,280,281,282], "osl": 300  },
//             { "event": "turn", "deg": 270, "dir": true },
//             { "event": "run", "dir": false, "route": [171,151] },
//             { "event": "lift", "dir": 1 },
//             { "event": "run", "dir": true, "route": [176] },
//             { "event": "turn", "deg": 180, "dir": false }

// loop([
//             { "event": "run", "dir": true, "route": [278,152,280,281,282], "osl": 160  },
//             { "event": "turn", "deg": 270, "dir": true },
//             { "event": "run", "dir": false, "route": [175,153] },
//             { "event": "lift", "dir": 1 },
//             { "event": "run", "dir": true, "route": [292] },
//             { "event": "turn", "deg": 180, "dir": false }
// 	]);
	// loop([
    //         { "event": "run", "dir": true, "route": [278,152,280,281,282, 283], "osl": 175  },
    //         { "event": "turn", "deg": 270, "dir": true },
    //         { "event": "run", "dir": false, "route": [298,93], "osl": -200 },//150
    //         { "event": "lift", "dir": 1 },
    //         { "event": "run", "dir": true, "route": [294] },
    //         { "event": "turn", "deg": 180, "dir": false }
	// ]);
	// loop([
    //         { "event": "run", "dir": true, "route": [278,152,280,281,282, 283], "osl": 0  },
    //         { "event": "turn", "deg": 270, "dir": true },
    //         { "event": "run", "dir": false, "route": [290,177] },
    //         { "event": "lift", "dir": 1 },
    //         { "event": "run", "dir": true, "route": [289] },
    //         { "event": "turn", "deg": 180, "dir": false }
	// ]);
	// loop([
    //         { "event": "run", "dir": true, "route": [269,270,271,272], "osl": 40  },
    //         { "event": "turn", "deg": 270, "dir": true },
    //         { "event": "run", "dir": false, "route": [301,150] },
    //         { "event": "lift", "dir": 1 },
    //         { "event": "run", "dir": true, "route": [302] },
    //         { "event": "turn", "deg": 180, "dir": false }
	// ]);
	// loop([
    //         { "event": "run", "dir": true, "route": [269,270,271,272], "osl": 200  },
    //         { "event": "turn", "deg": 270, "dir": true },
    //         { "event": "run", "dir": false, "route": [303,300] },
    //         { "event": "lift", "dir": 1 },
    //         { "event": "run", "dir": true, "route": [304] },
    //         { "event": "turn", "deg": 180, "dir": false }
	// ]);
	// loop([
    //         { "event": "run", "dir": true, "route": [269,270,271], "osl": 85  },
    //         { "event": "turn", "deg": 270, "dir": true },
    //         { "event": "run", "dir": false, "route": [306,305] },
    //         { "event": "lift", "dir": 1 },
    //         { "event": "run", "dir": true, "route": [307] },
    //         { "event": "turn", "deg": 180, "dir": false }
	// ]);
	// loop([
    //         { "event": "run", "dir": true, "route": [269,270,271], "osl": 235  },
    //         { "event": "turn", "deg": 270, "dir": true },
    //         { "event": "run", "dir": false, "route": [309,308] },
    //         { "event": "lift", "dir": 1 },
    //         { "event": "run", "dir": true, "route": [310] },
    //         { "event": "turn", "deg": 180, "dir": false }
	// ]);
}

let clearCurrentJob = ()=>{
	global.log('Clear Current Job.');
	step = 100;
	if(turnInter){ clearInterval(turnInter); }
	if(runInter){ clearInterval(runInter); }
	if(offsetInter){ clearInterval(offsetInter); }
	if(waitTime){ clearTimeout(waitTime); }
	if(jobTime){ clearTimeout(jobTime); }
	global.var.route = [];
	global.var.to = null;
	global.var.buffer = { wipin: null, pumpin: null };
	move.stop();
}

